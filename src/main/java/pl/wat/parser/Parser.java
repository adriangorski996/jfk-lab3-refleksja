package pl.wat.parser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Parser {

    private List<File> getJarFiles(String dirPath) throws Exception {
        File file = new File(dirPath);
        File[] matchingFiles = file.listFiles((dir, name) -> name.endsWith("jar"));
        if ((matchingFiles != null ? matchingFiles.length : 0) == 0) throw new Exception("There is no jar files in this directory");
        return Arrays.asList(matchingFiles);
    }

    private List<Class> getEntriesFromJar(File jarPath) throws IOException {
        List<Class> out = new ArrayList<>(4);
        JarFile jarFile = new JarFile(jarPath);
        Enumeration<JarEntry> entries = jarFile.entries();
        URL[] urls = {new URL("jar:file:" + jarPath + "!/")};
        URLClassLoader cl = URLClassLoader.newInstance(urls);

        while (entries.hasMoreElements()) {
            JarEntry je = entries.nextElement();
            if (je.isDirectory() || !je.getName().endsWith(".class")) {
                continue;
            }


            String className = je.getName().substring(0, je.getName().length() - 6);
            className = className.replace('/', '.');

            try {
                Class<?> c = cl.loadClass(className);
                out.add(c);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return out;
    }

    public List<Class> findClassesInDir(String path) throws Exception {

        List<File> files = getJarFiles(path);
        List<Class> out = new ArrayList<>();
        files.forEach(r -> {
            try {
                out.addAll(getEntriesFromJar(r));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return out;


    }
}