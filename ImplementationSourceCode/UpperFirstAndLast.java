package pl.wat;

import pl.wat.interfaces.Description;
import pl.wat.interfaces.Upperable;

@Description(description = "Upper first and last letter in word")
public class UpperFirstAndLast implements Upperable {


    @Override
    public String toUpper(String str) {

        char[] out = str.toCharArray();
        out[0] = Character.toUpperCase(out[0]);
        out[out.length-1] = Character.toUpperCase(out[out.length-1]);

        return String.copyValueOf(out);
    }
}
