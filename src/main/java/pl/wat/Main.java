package pl.wat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Main extends Application {


    private static Stage stage = null;

    @Override
    public void start(Stage primaryStage) throws Exception {

        stage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/panel.fxml"));
        primaryStage.setTitle("Refleksja dla interfejsu Upperable");
        primaryStage.setScene(new Scene(root, 720, 488));
        primaryStage.show();
    }


    public static void main(String[] args){

        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }
}
