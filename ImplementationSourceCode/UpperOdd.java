package pl.wat;

import pl.wat.interfaces.Description;
import pl.wat.interfaces.Upperable;

@Description(description = "Upper odd letter in word")
public class UpperOdd implements Upperable {

    @Override
    public String toUpper(String str) {
        char[] out = str.toCharArray();

        for (int i = 0; i < out.length; i++) {
            if (i % 2 == 0) {
                out[i] = Character.toUpperCase(out[i]);
            }
        }

        return String.copyValueOf(out);
    }

}
