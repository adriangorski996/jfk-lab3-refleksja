package pl.wat.interfaces;

public interface Upperable {

    String toUpper(String str);

}
