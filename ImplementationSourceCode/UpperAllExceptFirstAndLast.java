package pl.wat;

import pl.wat.interfaces.Description;
import pl.wat.interfaces.Upperable;

@Description(description = "Upper all letters except first and last")
public class UpperAllExceptFirstAndLast implements Upperable {

    @Override
    public String toUpper(String str) {
        str = str.toUpperCase();
        char[] out = str.toCharArray();
        out[0] = Character.toLowerCase(out[0]);
        out[out.length-1] = Character.toLowerCase(out[out.length-1]);

        return String.copyValueOf(out);
    }
}
