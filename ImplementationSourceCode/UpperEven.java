package pl.wat;

import pl.wat.interfaces.Description;
import pl.wat.interfaces.Upperable;


@Description(description = "Upper even letter in word")
public class UpperEven implements Upperable {

    @Override
    public String toUpper(String str) {

        char[] out = str.toCharArray();

        for (int i = 0; i<out.length; i++){
            if(i%2==1){
                out[i] = Character.toUpperCase(out[i]);
            }
        }

        return String.copyValueOf(out);
    }
}
