package pl.wat.contoller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import pl.wat.Main;
import pl.wat.interfaces.Description;
import pl.wat.interfaces.Upperable;
import pl.wat.parser.Parser;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {


    @FXML
    private Button chooseDir;

    @FXML
    private Button start;

    @FXML
    private TextArea metadata;

    @FXML
    private TextField parameter;

    @FXML
    private Text results;

    @FXML
    private ListView<Class> classList = new ListView<>();

    @FXML
    private Text info;
    private ObservableList<Class> data = FXCollections.observableArrayList();
    private Parser parser = new Parser();

    private Class choosedClass = null;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        if (choosedClass == null) {
            start.setDisable(true);
        }

        chooseDir.setOnAction(r -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File selectedDirectory = directoryChooser.showDialog(Main.getStage());

            if (selectedDirectory == null) {
                metadata.setText("No Directory selected");
            } else {

                try {
                    this.data.clear();
                    this.data.addAll(parser.findClassesInDir(selectedDirectory.getAbsolutePath()));
                    this.classList.setItems(data);
                } catch (Exception e) {
                    metadata.setText(e.getMessage());
                }

            }

        });


        classList.setOnMouseClicked(e -> {
            if (classList.getSelectionModel().getSelectedItem() != null) {
                choosedClass = classList.getSelectionModel().getSelectedItem();
                loadClassToMetadataArea();
                if (Upperable.class.isAssignableFrom(choosedClass) && !choosedClass.isInterface()) {
                    start.setDisable(false);
                    info.setText("");
                }
                if (!Upperable.class.isAssignableFrom(choosedClass) || choosedClass.isInterface()) {
                    start.setDisable(true);
                    info.setText("Przycisk uaktywnia się po wybraniu klasy implementującej interfejs Upperable.");
                }

            }
        });

        start.setOnMouseClicked(e -> {
            Upperable upperable = null;
            try {
                upperable = (Upperable) choosedClass.newInstance();
            } catch (InstantiationException | IllegalAccessException e1) {
                results.setText(e1.getMessage());
            }
            if (!parameter.getText().equals("") && upperable != null) {
                    results.setText(upperable.toUpper(parameter.getText()));

            } else {
                results.setText("Wpisz tekst w polu \"Parametr\" ");
            }
        });


    }

    private void loadClassToMetadataArea() {
        StringBuilder sb = new StringBuilder();
        sb.append("Class name: ").append(choosedClass.getName()).append("\n");
        sb.append("Has annotations?: ").append(choosedClass.getAnnotations().length != 0 ? "YES" : "NO").append("\n");
        if (choosedClass.isAnnotationPresent(Description.class)) {
            sb.append("Description: ").append(((Description) choosedClass.getAnnotation(Description.class)).description()).append("\n");
        }

        if (choosedClass.getSuperclass() != null) {
            sb.append("\nExtends class: ").append(choosedClass.getSuperclass());
        }

        if (choosedClass.getInterfaces().length != 0) {
            sb.append("\nImplements interfaces: ");
            for (Class d : choosedClass.getInterfaces()) {
                sb.append("\n\t ").append(d.toString());
            }
        }

        if (choosedClass.getFields().length != 0) {
            sb.append("\nFields: ");
            for (Field d : choosedClass.getDeclaredFields()) {
                sb.append("\n\t ").append(d.toString());
            }
        }

        if (choosedClass.getMethods().length != 0) {
            sb.append("\nMethods: ");
            for (Method m : choosedClass.getDeclaredMethods()) {
                sb.append("\n\t ").append(m.toString());
            }
        }

        this.metadata.setText(sb.toString());


    }
}
